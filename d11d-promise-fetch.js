"use strict";

// Exemple avec fetch qui retourne une promesse par défaut
fetch('https://flash.cegepgarneau.ca/api/publications/activites.json')
.then((response) => {
  return response.json();
})
.then((result) => { 
  console.log('result', result);
})
.catch((err) => {
  console.log('err', err);
})
