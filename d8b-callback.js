"use strict";

// Exemple de callback asynchrone

function download(image, callback) {
    // Délai d'attente volontaire pour simuler une requête (de 0 à 10000 ms).
    let delai = Math.floor(Math.random() * 10000);
    // On en profite pour voir la notation avec les backticks`
    console.log(`Téléchargement ${image} ...`);
    setTimeout(() => {
        // Télécharge une image
        // ...
        // Exécute une autre fonction quand l'image est bien téléchargée
        callback(image);
    }, delai);
}

function process(picture) {
    console.log(`Exécution du traitement de l'image ${picture}`);
}

const url = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg'];

for (let i = 0; i < url.length; i++) {
    download(url[i], process);
}

console.log('Terminé');

// Exemple avec map()
// url.map(image => download(image, process));
