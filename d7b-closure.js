"use strict";

const tab = [];// Tableau pour stocker les fonctions

function compteur() {
    for (var i = 0; i < 5; i++) { /// Remplacer 'var' par 'let' et comparer. 
        tab.push(function () { 
            console.log('fonction indice i = ', i);
        });
    }
}



compteur();

// Exécution des fonctions stockées dans `tab`
tab.forEach( (uneFonctionDeTab) => {
    uneFonctionDeTab();
});

/*Problème avec 'var' :
'var' crée une variable accessible en dehors de la boucle.
Ainsi 'var' n’a pas de portée de bloc, il est fonctionnel, ce qui signifie que toutes les fonctions stockées dans tab référencent la même variable i.
Au moment où les fonctions s’exécutent, la boucle est déjà terminée et i = 5.
Résultat : Chaque fonction affiche 5 au lieu de 0, 1, 2, 3, 4.
*/

/*Solution avec 'let' :
let a une portée de bloc, donc chaque itération crée une nouvelle variable i spécifique à cette itération.
Chaque fonction stockée dans tab capture la bonne valeur de i au moment de son ajout.
*/


// Autre exemple
for (var i = 1; i < 5; i++) {  // remplacer 'var' par 'let' et comparer
    setTimeout(() => console.log(i), 1000);  // 5 5 5 5 5
}

/*Problème avec 'var'
setTimeout s’exécute après la fin de la boucle, donc lorsque les callbacks s’exécutent, la valeur de i est déjà 5.
*/

/*Solution avec 'let'
let crée une nouvelle variable i pour chaque itération, donc chaque setTimeout capture une valeur différente.
*/