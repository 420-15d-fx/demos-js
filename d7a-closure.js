"use strict"; 

const myFunction = () => {
    let myValue = 2; // Déclare une variable locale à myFunction

    const childFunction = () => {
        console.log(myValue += 1); // Incrémente myValue et l'affiche
    };

    return childFunction; // Retourne la fonction childFunction sans l'exécuter
};

// Appelle myFunction et stocke son retour (childFunction) dans result
const result = myFunction();

// Exécute result(), qui est en réalité childFunction()
// Comme childFunction garde une référence à myValue, elle continue à l'incrémenter à chaque appel
result(); // Affiche 3
result(); // Affiche 4
result(); // Affiche 5


