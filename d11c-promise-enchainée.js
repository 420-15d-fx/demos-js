"use strict";

const getData = (x) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('x : ', x);
      resolve(x + 1);
    }, 1000);
  });
};

console.log('Je suis la ligne 12')

let x;

getData(10)
.then((result) => {
  x = result;
  return getData(15);
})
.then((result) => {
  var y = x + result;
  return getData(y);
})
.then((result) => {
  console.log('result', result);
})
.catch(() => {
  console.log('err');
});


console.log("Je suis la ligne 33")